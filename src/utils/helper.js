
let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
    var pm_a = parseInt(a["PM2.5"]);
    var pm_b = parseInt(b["PM2.5"]);
    if(isNaN(pm_a))
	{
	    pm_a = 0;
	}
    if(isNaN(pm_b))
	{
	    pm_b = 0;
	}

    if(pm_a < pm_b)
	{
            return -1;
	}
    else 
	{
	    return 1;
	}
}

let average = (nums) => {
	let total = 0;
	for(let i = 0; i < nums.length; i++)
	{
		if(!isNaN(nums[i]))
		{
		    total += nums[i];
		}
	}
	let avg = total / nums.length;
	return Math.round(avg * 100) / 100;
}


module.exports = {
    sorting,
    compare,
    average
}
