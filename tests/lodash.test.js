const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

test('Should chunk an array', () => {
    const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    let chunkArr = _.chunk(array, 3);
    expect(chunkArr).to.eql([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]]);
})

test('Should compact an array', () => {
    const array = [1, false, null, 2, 3, 4, 0, "", 5, 6, NaN, 7, 8, undefined];
    let compactArr = _.compact(array);
    expect(compactArr).to.eql([1, 2, 3, 4, 5, 6, 7, 8]);
})

test('Should concat an array', () => {
    const array = [1, 2, 3];
    let concatArr = _.concat(array, 2, [3], [[4]]);
    expect(concatArr).to.eql([1, 2, 3, 2, 3, [4]]);
})

test('Should drop an array', () => {
    const array = [1, 2, 3, 4, 5, 6];
    let dropArr = _.drop(array, 3);
    expect(dropArr).to.eql([4, 5, 6]);
})

test('Should fill an array', () => {
    const array = [1, 2, 3, 4, 5, 6];
    let fillArr = _.fill(array, "*", 2, 5);
    expect(fillArr).to.eql([1, 2, "*", "*", "*", 6]);
})

test('Get last element of an array', () => {
    const array = [1, 2, 3];
    let last = _.last(array);
    expect(last).to.eql(3);
})

test('Should join an array', () => {
    const array = [1, 2, 3];
    let joinArr = _.join(array, "@");
    expect(joinArr).to.eql("1@2@3");
})

test('Delete elements from an array', () => {
    const array = [1, 2, 3, 1, 3, 4, 5];
    let withoutArr = _.without(array, 2, 3);
    expect(withoutArr).to.eql([1, 1, 4, 5]);
})

test('Get the max element of an array', () => {
    const array = [1, 2, 3];
    let max = _.max(array);
    expect(max).to.eql(3);
})

test('Get the size of an array', () => {
    const array = [1, 2, 3];
    let size = _.size(array);
    expect(size).to.eql(3);
})




